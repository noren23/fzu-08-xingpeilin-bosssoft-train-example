package com.bosssoft.hr.train.j2se.basic.example.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:25
 **/
public class NIOServerSocket implements Starter {

    private static final Logger log = LoggerFactory.getLogger(NIOServerSocket.class);

    @Override
    public boolean start() {
        ServerSocketChannel serverSocketChannel = null;
        Selector selector = null;
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(9898));
            serverSocketChannel.configureBlocking(false);
            selector = Selector.open();
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            log.info("服务端开始工作");

            while (selector.select() > 0) {
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey sk = iterator.next();
                    if (sk.isAcceptable()) {
                        SocketChannel socketChannel = serverSocketChannel.accept();
                        socketChannel.configureBlocking(false);
                        socketChannel.register(selector, SelectionKey.OP_READ);
                    } else if (sk.isReadable()) {
                        SocketChannel socketChannel = (SocketChannel) sk.channel();
                        ByteBuffer buffer = ByteBuffer.allocate(1024);
                        int len;
                        while ((len = socketChannel.read(buffer)) > 0) {
                            buffer.flip();
                            String context = new String(buffer.array(), 0, len);
                            log.info(context);
                            buffer.clear();
                        }
                        buffer.put("Hello Client".getBytes());
                        buffer.flip();
                        socketChannel.write(buffer);

                        socketChannel.close();
                    }
                    iterator.remove();
                }
            }

            return true;
        } catch (IOException e) {
            log.error("socket连接异常:", e);
        } finally {
            try {
                assert serverSocketChannel != null;
                serverSocketChannel.close();
                assert selector != null;
                selector.close();
            } catch (IOException e) {
                log.error("socket关闭异常:", e);
            }
        }
        return false;
    }
}
