package com.bosssoft.hr.train.j2se.basic.example.annotation;

import com.bosssoft.hr.train.j2se.basic.example.database.DBUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @description:
 * @author: 西西西瓜萌
 * @create: 2020-06-01 12:03
 **/
public abstract class BaseModel {
    private static final Logger log = LoggerFactory.getLogger(BaseModel.class);
    /**
     * 数据库列名对属性字段名转换
     */
    private Map<String, String> colToField = new HashMap<>();
    /**
     * 表名
     */
    private String tableName = null;
    /**
     * 主键名
     */
    private String primaryKey = null;
    /**
     * 所有列名
     */
    private StringBuilder allColumns = new StringBuilder();

    /**
     * 解析注解
     * 默认注解正确
     *
     *
     */
    public Map<String, String> analysisAnnotation() {
        Map<String, String> map = new LinkedHashMap<>(20);
        try {
            Class<?> cls = this.getClass();
            boolean isTable = cls.isAnnotationPresent(Table.class);
            if (isTable) {
                tableName = cls.getAnnotation(Table.class).value();
            } else {
                log.error("缺少@Table注解");
                return null;
            }
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                boolean isId = field.isAnnotationPresent(Id.class);
                boolean isColumn = field.isAnnotationPresent(Column.class);
                if (isId) {
                    primaryKey = field.getAnnotation(Id.class).value();
                    map.put(primaryKey, field.get(this).toString());
                    allColumns.append(primaryKey).append(",");
                    colToField.put(primaryKey, field.getName());
                } else if (isColumn) {
                    String column = field.getAnnotation(Column.class).value();
                    map.put(column, field.get(this).toString());
                    allColumns.append(column).append(",");
                    colToField.put(column, field.getName());
                } else {
                    //不做处理
                }
            }
            allColumns = new StringBuilder(allColumns.substring(0, allColumns.lastIndexOf(",")));
            return map;
        } catch (Exception e) {
            log.error("反射解析异常:", e);
        }
        return null;
    }


    /**
     * 保存
     *
     *
     */
    public int save() {
        Map<String, String> map = analysisAnnotation();
        int numColumns = allColumns.toString().split(",").length;
        StringBuilder preSql = new StringBuilder();
        for (int i = 0; i < numColumns; i++) {
            preSql.append("?");
            if (i < numColumns - 1) {
                preSql.append(",");
            }
        }
        String sql = "insert into " + tableName + "(" + allColumns + ")" + " values(" + preSql + ")";
        Connection conn = DBUtil.init();
        PreparedStatement ps = null;
        try {
            assert conn != null;
            ps = conn.prepareStatement(sql);
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            int index = 0;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                String value = entry.getValue();
                ps.setString(++index, value);
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            log.error("语句执行异常:", e);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.error("database关闭异常:", e);
                }
            }
            DBUtil.closeConn();
        }
        return 0;
    }

    /**
     * 更新
     *
     */
    public int update() {
        Map<String, String> map = analysisAnnotation();
        String[] columns = allColumns.toString().split(",");
        StringBuilder sql = new StringBuilder("update " + tableName + " set ");
        for (String column : columns) {
            if (!primaryKey.equals(column)) {
                sql.append(column).append("=? ,");
            }
        }
        sql = new StringBuilder(sql.substring(0, sql.lastIndexOf(",")));
        sql.append("where ").append(primaryKey).append(" = ? ");
        Connection conn = DBUtil.init();
        PreparedStatement ps = null;
        try {
            assert conn != null;
            ps = conn.prepareStatement(sql.toString());
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            int index = 0;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                String key = entry.getKey();
                String value = entry.getValue();
                if (primaryKey.equals(key)) {
                    ps.setString(columns.length, value);
                } else {
                    ps.setString(++index, value);
                }
            }
            return ps.executeUpdate();
        } catch (SQLException e) {
            log.error("更新异常:", e);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.error("关闭异常:", e);
                }
            }
            DBUtil.closeConn();
        }
        return 0;
    }

    /**
     * 按照id删除
     *
     *
     */
    public int remove() {
        Map<String, String> map = analysisAnnotation();
        String id = map.get(primaryKey);
        String sql = "delete from " + tableName + " where " + primaryKey + " =  ? ";
        DBUtil.init();
        try {
            return DBUtil.creatOrUpdateOrDelete(sql, id);
        } finally {
            DBUtil.closeConn();
        }
    }

    /**
     * 查找 默认全部搜索了
     *
     */
    public List<Object> queryForList() {
        analysisAnnotation();
        String sql = "select *  from " + tableName + "";
        DBUtil.init();
        ResultSet resultSet = DBUtil.select(sql);
        List<Object> result = new ArrayList<>();
        try {
            int columnCount = resultSet.getMetaData().getColumnCount();
            while (resultSet.next()) {
                BaseModel instance = this.getClass().newInstance();
                for (int i = 1; i <= columnCount; i++) {
                    Field field = instance
                            .getClass()
                            .getDeclaredField(colToField.get(resultSet.getMetaData().getColumnName(i)));
                    field.setAccessible(true);
                    field.set(instance, resultSet.getObject(i));
                }
                result.add(instance);
            }
        } catch (Exception e) {
            log.error("database异常:", e);
        } finally {
            DBUtil.closeConn();
        }
        log.info("查找结果：{}", result);
        return result;
    }
}
