package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description: 该类主要是为了测试 ConcurrentHashmap 用上此类不用担心并发问题了
 * @author: Administrator
 * @create: 2020-05-28 20:58
 **/
@Slf4j
public class ConcurrentHashMapExampleImpl implements HashMapExample<Role, Resource> {
    /**
     * 定义 ConcurrentHashMap对象用于测试
     */
    private Map<Role, Resource> concurrentMap = new ConcurrentHashMap<>();

    /**
     * 然后该方法不用加 synchronized 也是线程安全，但是操作并不因为锁定，这个就是
     * 该类对hashtable的改进
     *
     * @param key   map的键
     * @param value 值
     * @return 返回的是前一个的值或者是null
     */
    @Override
    public Resource put(Role key, Resource value) {
        return concurrentMap.put(key, value);
    }

    @Override
    public Resource remove(Role key) {
        return concurrentMap.remove(key);
    }

    @Override
    public boolean containsKey(Role key) {
        return concurrentMap.containsKey(key);
    }

    @Override
    public void visitByEntryset() {
        Set<Map.Entry<Role, Resource>> set = concurrentMap.entrySet();
        for (Map.Entry<Role, Resource> entry : set) {
            log.info(Constraint.LOG_TAG, entry.getKey() + "####" + entry.getValue());
        }
    }

    /**
     * 请比较 传统的for遍历和 lambda 的 forEach 代码简洁效率高，
     * 请在JDK1.8以上的代码中执行
     */
    public void visitByEntrySetLambda() {
        Set<Map.Entry<Role, Resource>> set = concurrentMap.entrySet();
        set.forEach(entry -> log.info(Constraint.LOG_TAG, entry.getKey() + "####" + entry.getValue()));
    }


    @Override
    public void visitByKeyset() {
        if (null != concurrentMap) {
            Set<Role> set = concurrentMap.keySet();
            Iterator<Role> iterator = set.iterator();
            while (iterator.hasNext()) {
                Role role = iterator.next();
                log.info(Constraint.LOG_TAG, role);
                log.info(Constraint.LOG_TAG, concurrentMap.get(role));
            }
        }
    }

    @Override
    public void visitByValues() {
        Collection<Resource> collection = concurrentMap.values();
        Iterator<Resource> iterator = collection.iterator();
        while (iterator.hasNext()) {
            Resource resource = iterator.next();
            log.info(Constraint.LOG_TAG, resource);
        }
    }
}
