package com.bosssoft.hr.train.j2se.basic.example.collection;



/**
 * @description: 如果需要添加HashMap的特有的接口测试就需要在该接口中添加
 * 需要测试的方法
 * @author: Administrator
 * @create: 2020-05-28 20:45
 **/
public interface HashMapExample<X, Y> extends MapExample<X, Y> {
}
