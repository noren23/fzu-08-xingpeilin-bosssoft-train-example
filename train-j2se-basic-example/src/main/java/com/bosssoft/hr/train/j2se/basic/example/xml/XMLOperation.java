package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:06
 **/
public interface XMLOperation<T extends Student> {
    /**
     * 增加
     * @param object
     * @return boolean
     */
    boolean create(T object);
    /**
     * 删除
     * @param object
     * @return boolean
     */
    boolean remove(T object);
    /**
     * 修改
     * @param object
     * @return boolean
     */
    boolean update(T object);
    /**
     * 查询
     * @param object
     * @return T
     */
    T query(T object);

}
