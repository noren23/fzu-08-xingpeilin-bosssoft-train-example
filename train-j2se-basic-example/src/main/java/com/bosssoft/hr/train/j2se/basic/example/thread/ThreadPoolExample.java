package com.bosssoft.hr.train.j2se.basic.example.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/5/30--16:59
 */
public class ThreadPoolExample {

    public ThreadPoolExecutor testThreadPool() {
        return new ThreadPoolExecutor(5,
                10,
                60,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                new ThreadPoolExecutor.DiscardPolicy());

    }
}
