package com.bosssoft.hr.train.j2se.basic.example.socket;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:16
 **/
public class ServerSocketApplication {
    public static  void main(String[] args){
        Starter serverSocket=new NIOServerSocket();
        serverSocket.start();
    }
}
