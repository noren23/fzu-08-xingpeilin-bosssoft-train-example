package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class LinkedListExampleImplTest {
    LinkedListExampleImpl linkedListExample;
    User user;
    User user2;

    @Before
    public void setUp() {
        linkedListExample = new LinkedListExampleImpl();
        user = new User(1, "zhangsan");
        user2 = new User(2, "lisi");
    }

    @After
    public void tearDown() {
        linkedListExample = null;
        user = null;
        user2 = null;
    }

    @Test
    public void append() {
        //正常添加
        assertTrue(linkedListExample.append(user));
        // 添加为null
        assertTrue(linkedListExample.append(null));
    }

    @Test
    public void get() {
        //首先插入
        assertTrue(linkedListExample.append(user));
        //正常查询
        assertEquals(user, linkedListExample.get(0));

        //超出范围为null
        assertNull(linkedListExample.get(-1));
    }

    @Test
    public void insert() {
         assertTrue(linkedListExample.insert(0, user));
        assertFalse(linkedListExample.insert(3, user));
        assertFalse(linkedListExample.insert(-1, user));
    }

    @Test
    public void remove() {
        assertTrue(linkedListExample.insert(0, user));
        assertTrue(linkedListExample.insert(1, user));
        assertTrue(linkedListExample.remove(0));
        assertFalse(linkedListExample.remove(-1));
        assertFalse(linkedListExample.remove(5));
    }

    @Test
    public void listByIndex() {
        assertTrue(linkedListExample.insert(0, user));
        assertTrue(linkedListExample.insert(1, user));
        try {
            linkedListExample.listByIndex();
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void listByIterator() {
         assertTrue(linkedListExample.insert(0, user));
        assertTrue(linkedListExample.insert(1, user2));
        try {
            linkedListExample.listByIterator();
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void toArray() {
          assertTrue(linkedListExample.insert(0, user));
        assertTrue(linkedListExample.insert(1, user2));
        User[] users = new User[]{user, user2};
        assertArrayEquals(users, linkedListExample.toArray());
    }

    @Test
    public void sort() {
        assertTrue(linkedListExample.append(user2));
        assertTrue(linkedListExample.append(user));
        linkedListExample.sort();
        assertEquals(user, linkedListExample.get(0));
        assertEquals(user2, linkedListExample.get(1));
    }


    @Test
    public void addFirst() {
        linkedListExample.addFirst(user);
        assertEquals(user, linkedListExample.get(0));
    }

    @Test
    public void offer() {
        linkedListExample.offer(user);
        linkedListExample.offer(user2);
        assertEquals(user2, linkedListExample.get(1));
    }

    @Test
    public void synchronizedVisit() {
        // TODO
        linkedListExample.synchronizedVisit(user);
        assertEquals(user, linkedListExample.get(0));
    }

    @Test
    public void push() {
        linkedListExample.push(user);
        assertEquals(user, linkedListExample.get(0));
    }

    @Test
    public void pop() {
        linkedListExample.push(user);
        assertEquals(user, linkedListExample.pop());
    }
}