package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class ConcurrentHashMapExampleImplTest {
    ConcurrentHashMapExampleImpl concurrentHashMapExample;
    Role role;
    Resource resource;

    @Before
    public void setUp() {
        concurrentHashMapExample = new ConcurrentHashMapExampleImpl();
        role = new Role(1, "boss");
        resource = new Resource(1, "all");
    }

    @After
    public void tearDown() {
        concurrentHashMapExample = null;
        role = null;
        resource = null;
    }


    @Test(expected = NullPointerException.class)
    public void put() {

        concurrentHashMapExample.put(null, null);
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        Resource resource = concurrentHashMapExample.put(this.role, this.resource);
        assertEquals(this.resource, resource);
    }

    @Test
    public void remove() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        assertEquals(this.resource, concurrentHashMapExample.remove(role));
        assertNull(concurrentHashMapExample.remove(new Role()));

    }

    @Test
    public void containsKey() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        assertTrue(concurrentHashMapExample.containsKey(role));
        assertFalse(concurrentHashMapExample.containsKey(new Role()));

    }

    @Test
    public void visitByEntryset() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        concurrentHashMapExample.visitByEntryset();

    }

    @Test
    public void visitByEntrySetLambda() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        concurrentHashMapExample.visitByEntrySetLambda();
    }

    @Test
    public void visitByKeyset() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        concurrentHashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        assertNull(concurrentHashMapExample.put(this.role, this.resource));
        concurrentHashMapExample.visitByValues();
    }
}