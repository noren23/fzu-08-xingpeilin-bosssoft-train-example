package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



import static org.junit.Assert.*;

public class ArrayListExampleImplTest {
    ArrayListExampleImpl arrayListExample;
    User user1;
    User user2;

    @Before
    public void setUp() {
        arrayListExample = new ArrayListExampleImpl();
        user1 = new User(1, "zhangsan");
        user2 = new User(2, "lisi");
    }

    @After
    public void tearDown() {
        arrayListExample = null;
        user1=null;
        user2=null;
    }

    @Test
    public void append() {
        assertTrue(arrayListExample.append(user1));
        assertTrue(arrayListExample.append(null));


    }

    @Test
    public void get() {
        assertTrue(arrayListExample.append(user1));
        assertEquals(user1, arrayListExample.get(0));
        assertNull(arrayListExample.get(-1));

    }

    @Test
    public void insert() {
        assertTrue(arrayListExample.insert(0, user1));
        assertFalse(arrayListExample.insert(3, user1));
        assertFalse(arrayListExample.insert(-1, user1));

    }

    @Test
    public void remove() {
        assertTrue(arrayListExample.insert(0, user1));
        assertTrue(arrayListExample.insert(1, user1));
        assertTrue(arrayListExample.remove(0));
        assertFalse(arrayListExample.remove(-1));
        assertFalse(arrayListExample.remove(5));
    }

    @Test
    public void listByIndex() {
        assertTrue(arrayListExample.insert(0, user1));
        assertTrue(arrayListExample.insert(1, user1));
        try {
            arrayListExample.listByIndex();
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }

    }

    @Test
    public void listByIterator() {
        assertTrue(arrayListExample.insert(0, user1));
        assertTrue(arrayListExample.insert(1, user2));
        try {
            arrayListExample.listByIterator();
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void toArray() {
        assertTrue(arrayListExample.insert(0, user1));
        assertTrue(arrayListExample.insert(1, user2));
        User[] users = new User[]{user1, user2};
        assertArrayEquals(users, arrayListExample.toArray());

    }

    @Test
    public void sort() {
        assertTrue(arrayListExample.append(user2));
        assertTrue(arrayListExample.append(user1));
        arrayListExample.sort();
        assertEquals(user1, arrayListExample.get(0));
        assertEquals(user2, arrayListExample.get(1));
    }

}