package com.bosssoft.hr.train.j2se.basic.example.thread;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ThreadPoolExecutor;


public class ThreadPoolExampleTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testThreadPool() {
        ThreadPoolExample threadPoolExample = new ThreadPoolExample();
        ThreadPoolExecutor executor = threadPoolExample.testThreadPool();
        for(int i=0;i<20;i++){
            Task task=new Task(i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executor.execute(task);
        }
    }
}