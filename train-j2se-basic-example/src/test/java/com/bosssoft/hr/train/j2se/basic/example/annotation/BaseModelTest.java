package com.bosssoft.hr.train.j2se.basic.example.annotation;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BaseModelTest {
    UserModel userModel;

    @Before
    public void setUp() {
        userModel = new UserModel(226, "zyx", 28);
    }

    @After
    public void tearDown() {
        userModel = null;
    }

    @Test
    public void save() {
        userModel.setID(233);
        assertEquals(1, userModel.save());
        assertEquals(1, userModel.remove());
    }

    @Test
    public void update() {
        userModel.setName("张加帅");
        assertEquals(1, userModel.update());

    }

    @Test
    public void remove() {
        userModel = new UserModel(227, "zhaowu", 33);
        Assert.assertEquals(1, userModel.save());
        assertEquals(1, userModel.remove());

    }

    @Test
    public void queryForList() {
        userModel.queryForList();
    }
}