let str = "test2.js";
let num = 100;
let testFunction = function (num1,num2) {
    return num1 + num2;
}
class TestClass{
    constructor(name,id){
        this.name = name;
        this.id = id;
    }
    get getId(){
        return this.id;
    }
    set setId(newid){
        this.id = newid;
    }
    get getName(){
        return this.name;
    }
    set setName(newName){
        this.name = newName;
    }
}
let testclass = new TestClass("kiki",44);
export {str,num,testFunction,testclass};