package com.bosssoft.hr.train.service;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/9--10:38
 */
public interface SendEmailService {
    /**
     * 发送邮件
     */
    Boolean sendEmail();
}
