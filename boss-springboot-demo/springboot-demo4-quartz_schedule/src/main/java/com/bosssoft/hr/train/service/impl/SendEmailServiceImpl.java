package com.bosssoft.hr.train.service.impl;

import com.bosssoft.hr.train.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/9--10:39
 */
@Service
public class SendEmailServiceImpl implements SendEmailService {
    @Value("${spring.mail.username}")
    private String fromEmail;


    @Autowired
    private JavaMailSender javaMailSender;


    @Override
    @Scheduled(cron = "0 0/30 * * * ?")
    public Boolean sendEmail() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo("1069530206@qq.com");
        simpleMailMessage.setFrom(fromEmail);
        simpleMailMessage.setSubject("定时任务测试");
        simpleMailMessage.setText("这是一个定时任务发送的邮件");
        try {
            javaMailSender.send(simpleMailMessage);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
