var websocket = null;
if ('WebSocket' in window) {
    websocket = new WebSocket('ws://localhost:8082/demo2/websocket');
} else {
    alert('该浏览器不支持websocket!');
}
websocket.onopen = function (event) {
    console.log('建立连接');
}
websocket.onclose = function (event) {
    console.log('连接关闭');
}
websocket.onmessage = function (event) {
    let message = event.data
    $("#receiveText").val(message)
    console.log(event)
    console.log('收到消息:' + message);

}
websocket.onerror = function () {
    console.log('发生错误！');
}
window.onbeforeunload = function () {
    websocket.close();
}


function sendMessage(message) {
    websocket.send(message)
    console.log('发送消息:' + message);

}