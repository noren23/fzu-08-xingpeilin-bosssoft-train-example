package com.bosssoft.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDemo2WebsocketServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDemo2WebsocketServerApplication.class, args);
    }

}
