package com.bosssoft.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDemo3WebsericeClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDemo3WebsericeClientApplication.class, args);
    }

}
