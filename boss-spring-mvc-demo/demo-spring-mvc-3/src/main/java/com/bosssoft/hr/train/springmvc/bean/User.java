package com.bosssoft.hr.train.springmvc.bean;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/6--12:45
 */
public class User {
    private Integer id;
    private String name;


    public void setId(Integer id) {
        this.id = id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public User() {
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
