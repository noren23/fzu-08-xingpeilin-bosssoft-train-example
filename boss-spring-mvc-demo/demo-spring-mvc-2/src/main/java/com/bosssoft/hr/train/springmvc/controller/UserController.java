package com.bosssoft.hr.train.springmvc.controller;

import com.bosssoft.hr.train.springmvc.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/6--10:42
 */
@Controller
public class UserController {


    @GetMapping("/login")
    public String login(User user, Model model) {
        if (user == null || user.getUserName() == null) {
            model.addAttribute("err", "请登录");
            return "login";
        }
        if (user.getUserName().trim().length() > 0 && user.getUserName().equals(user.getPassword())) {
            model.addAttribute("user", user);
            return "hello";
        } else {
            model.addAttribute("err", "用户名或密码错误");
            return "login";
        }
    }

}
