package com.bosssoft.hr.train.springmvc.bean;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/6--16:04
 */
public class User {
    private Integer id;

    private String name;

    private Integer age;

    public Integer getId() {
        return id;
    }



    public String getName() {
        return name;
    }



    public Integer getAge() {
        return age;
    }


    public User() {
    }

    public User(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
