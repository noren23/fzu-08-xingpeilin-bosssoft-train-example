package com.bosssoft.hr.train.springmvc.controller;

import com.bosssoft.hr.train.springmvc.annotation.ProcessException;
import com.bosssoft.hr.train.springmvc.bean.User;
import com.bosssoft.hr.train.springmvc.exception.ExceptionCode;
import com.bosssoft.hr.train.springmvc.exception.ThrowErr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/6--16:10
 */
@RestController
public class UserController {
    @ProcessException
    @GetMapping("/get")
    public Object get(@RequestParam(value = "id", required = false) String id,
                    @RequestParam(value = "name", required = false) String name,
                    @RequestParam(value = "age", required = false) String age) {

        if (id == null || name == null || age == null) {
           return  ThrowErr.throwException(ExceptionCode.USER_INFO_LACK_EXCEPTION);
        }
        int userId = 0;
        try {
            userId = Integer.parseInt(id);
        } catch (Exception e) {
            return ThrowErr.throwException(ExceptionCode.USER_ID_FORMAT_EXCEPTION);
        }
        if (name.indexOf('#') != -1) {
            return ThrowErr.throwException(ExceptionCode.USER_NAME_EXCEPTION);
        }
        int userAge = 0;
        try {
            userAge = Integer.parseInt(age);
        } catch (Exception e) {
            return ThrowErr.throwException(ExceptionCode.USER_AGE_FORMAT_EXCEPTION);
        }
        if (userAge < 0 || userAge > 120) {
            return ThrowErr.throwException(ExceptionCode.USER_AGE_SCOPE_EXCEPTION);
        }

        return new User(userId, name, userAge);
    }

}
