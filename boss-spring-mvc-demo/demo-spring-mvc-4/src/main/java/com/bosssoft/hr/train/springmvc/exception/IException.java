package com.bosssoft.hr.train.springmvc.exception;

/**
 * @description: 自定义异常的接口
 * @author:西西西瓜萌
 * @time:2020/6/6--15:22
 */
public interface IException {

    Integer code();

    String msg();
}
