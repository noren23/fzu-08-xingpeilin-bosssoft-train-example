package com.bosssoft.hr.train.springmvc.exception;

/**
 * IException的枚举类
 * @description: ..
 * @author:西西西瓜萌
 * @
 */
public enum ExceptionCode implements IException {
    UNKNOWN_EXCEPTION(-1, "未知自定义异常"),
    USER_INFO_LACK_EXCEPTION(1000, "信息不完善"),
    USER_ID_FORMAT_EXCEPTION(1001, "ID不为整型数字"),
    USER_AGE_FORMAT_EXCEPTION(1002, "年龄不是整型数字"),
    USER_AGE_SCOPE_EXCEPTION(1003, "年龄范围异常"),
    USER_NAME_EXCEPTION(1004, "名字包含非法字符");


    private Integer code;

    private String msg;

    ExceptionCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer code() {
        return code;
    }

    @Override
    public String msg() {
        return msg;
    }
}
