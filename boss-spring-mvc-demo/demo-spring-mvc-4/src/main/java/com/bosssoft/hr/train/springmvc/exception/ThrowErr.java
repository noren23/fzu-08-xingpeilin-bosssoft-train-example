package com.bosssoft.hr.train.springmvc.exception;

import com.bosssoft.hr.train.springmvc.bean.Msg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/6--16:07
 */

@RestControllerAdvice
@Slf4j
public class ThrowErr {
    /**
     * 自定义异常
     *
     * @param exception
     */
    @ExceptionHandler(BaseException.class)
    public static Object throwException(IException exception) {
        log.error("IException,编码：{},消息:{}", exception.code(), exception.msg());

        return new Msg().code(exception.code()).msg(exception.msg());
    }

    /**
     * 基本异常
     *
     * @param exception
     */
    @ExceptionHandler(Exception.class)
    public static Object throwException(Exception exception) {
        log.error("Exception,编码：{},消息:{}", -1, exception.toString());
        return new Msg().code(-1).msg(exception.toString());
    }

    private ThrowErr() {
    }
}
