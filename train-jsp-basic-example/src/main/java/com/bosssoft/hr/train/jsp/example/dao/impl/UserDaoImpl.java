package com.bosssoft.hr.train.jsp.example.dao.impl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 **/
public class UserDaoImpl implements UserDao {
    @Override
    public int insert(User user) {
        if (user != null) {
            DBUtil.init();
            String sql;
            if (user.getId() != null) {
                sql = "insert into user " +
                        "values(" + "'" + user.getId() + "'" + "," + "'" + user.getName() + "'" + "," + "'" + user.getCode() + "'" + "," + "'" + user.getPassword() + "'" + ")";
            } else {
                sql = "insert into user(name,code,password) " +
                        "values(" + "'" + user.getName() + "'" + "," + "'" + user.getCode() + "'" + "," + "'" + user.getPassword() + "'" + ")";
            }
            int result = DBUtil.creatOrUpdateOrDelete(sql);
            DBUtil.closeConn();
            return result;
        }
        return 0;
    }

    @Override
    public int deleteById(Integer id) {
        if (id != null) {
            DBUtil.init();
            String sql = "delete from user where id = " + id;
            int result = DBUtil.creatOrUpdateOrDelete(sql);
            DBUtil.closeConn();
            return result;
        }
        return 0;
    }

    @Override
    public int update(User user) {
        Integer userId = user.getId();
        if (userId != null) {
            DBUtil.init();
            String sql = "update user " +
                    "set name=" + "'" + user.getName() + "'" + " , code=" + "'" + user.getCode() + "'" + " , password=" + "'" + user.getPassword() + "'" +
                    "where id=" + "'" + user.getId() + "'" + "";
            int result = DBUtil.creatOrUpdateOrDelete(sql);
            DBUtil.closeConn();
            return result;
        }
        return 0;
    }

    @Override
    public List<User> queryByCondition(Query query) {
        String sql = query.getUserByCodeAndPassword("user");
        if (sql == null) {
            sql = query.listNameLikes("user");
        }
        DBUtil.init();
        ResultSet resultSet = DBUtil.select(sql);
        List<User> users = new ArrayList<>();
        User user;
        try {
            while (resultSet.next()) {
                user = new User(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4));

                users.add(user);
            }
        } catch (SQLException e) {
            throw new BusinessException("10001", "数据库查询数据解析异常", e);
        } finally {
            DBUtil.closeConn();
        }
        return users;
    }
}
