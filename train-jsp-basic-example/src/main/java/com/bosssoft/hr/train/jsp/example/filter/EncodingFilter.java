package com.bosssoft.hr.train.jsp.example.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @description: 统一字符格式
 * @author:西西西瓜萌
 * @time:2020/5/30--22:04
 */
public class EncodingFilter implements Filter {


    private  String encoding;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding=filterConfig.getInitParameter("encode");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
        //nothing
    }
}
