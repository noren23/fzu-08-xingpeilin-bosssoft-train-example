package com.bosssoft.hr.train.jsp.example.service;

import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class UserServiceImplTest {

    private UserService userService;

    @Before
    public void setUp() {
        userService = new UserServiceImpl();
    }

    @After
    public void tearDown() {
        userService = null;
    }

    @Test
    public void save() {
        User user = new User(11111, "haha");
        assertTrue(userService.save(user));
        assertTrue(userService.remove(user));
        User user2 = new User();
        assertTrue(userService.save(user2));

    }

    /**
     * 测试插入重复
     */
    @Test(expected = BusinessException.class)
    public void saveSameId() {
        User user = new User(111, "haha");
        assertFalse(userService.save(user));
    }


    @Test
    public void remove() {
        User user0 = new User(11111, "haha");
        assertTrue(userService.save(user0));
        User user = new User(11111, "haha");
        assertTrue(userService.remove(user));

        assertFalse(userService.remove(user));
    }

    /**
     * 依据界面可能开展的测试编写测试用例  例如某一些字段为空等，实际就是依据
     * 项目需求的测试用例来扩展测试用例
     */
    @Test
    public void update() {
        User user = new User(1, "张三2", "zhangsan", "123456");
        assertTrue(userService.update(user));

        User user2 = new User(-1, "张三2", "zhangsan", "123456");
        assertFalse(userService.update(user2));
    }

    @Test
    public void queryByCondition() {
        User user = new User(-1, "");
        assertTrue(userService.queryByCondition(new Query(user.getName())).size() > 0);

    }

    /**
     * 测试正确账号和错误账号
     */
    @Test
    public void authentication() {
        User user = new User("zhangsan", "123456");
        assertTrue(userService.authentication(user));
        User user2 = new User("wangwu", "123456");
        assertFalse(userService.authentication(user2));

    }
}