package com.bosssoft.hr.train.test;

import com.bosssoft.hr.train.bean.User;
import com.bosssoft.hr.train.bean.UserExample;
import com.bosssoft.hr.train.dao.UserMapper;
import com.bosssoft.hr.train.util.MybatisUtil;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--21:53
 */
public class UserMapperTest {
    private static final Logger log = Logger.getLogger(UserMapperTest.class);
    /**
     * SqlSessionFactory
     */
    private static final SqlSessionFactory SQL_SESSION_FACTORY = MybatisUtil.getSqlSessionFactory();
    /**
     * SqlSession
     */
    private static final SqlSession SQL_SESSION = SQL_SESSION_FACTORY.openSession();

    /**
     * 测试批量插入
     */

    @Test
    public void testInsertUsers() {
        SqlSession sqlSession = SQL_SESSION_FACTORY.openSession(ExecutorType.BATCH);
        try {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            for (int i = 0; i < 10; i++) {
                mapper.insert(new User(UUID.randomUUID().toString().substring(0, 10), i % 2));
            }
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }

    }


    @Test
    public void testSelectUsers() {
        try {
            UserMapper mapper = SQL_SESSION.getMapper(UserMapper.class);

            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andLastNameLike("%%").andGenderEqualTo(0);
            List<User> users = mapper.selectByExample(example);
            log.info(users);

            UserExample example1 = new UserExample();
            criteria = example1.createCriteria();
            criteria.andGenderEqualTo(0);
            UserExample.Criteria criteria1 = example1.createCriteria();
            criteria1.andLastNameLike("%%");
            example1.or(criteria1);

            users = mapper.selectByExample(example);
            log.info(users);

        } finally {
            SQL_SESSION.close();
        }
    }

    @Test
    public void testUpdate() {
        try {
            UserMapper mapper = SQL_SESSION.getMapper(UserMapper.class);
            User user = new User();
            user.setId(1);
            user.setLastName("updateByPrimaryKeySelective");
            mapper.updateByPrimaryKeySelective(user);

            user = new User("updateByExample", 1);
            user.setId(1000);
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andIdEqualTo(2);

            mapper.updateByExample(user, example);

            SQL_SESSION.commit();
        } finally {
            SQL_SESSION.close();
        }
    }

}
