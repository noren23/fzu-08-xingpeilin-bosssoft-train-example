package com.bosssoft.hr.train.bean;

public class User {

    private Integer id;


    private String lastName;


    private Integer gender;





    public void setId(Integer id) {
        this.id = id;
    }





    public void setLastName(String lastName) {
        this.lastName = lastName == null ? null : lastName.trim();
    }







    public User() {
    }

    public User(String lastName, Integer gender) {
        this.lastName = lastName;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                '}';
    }
}