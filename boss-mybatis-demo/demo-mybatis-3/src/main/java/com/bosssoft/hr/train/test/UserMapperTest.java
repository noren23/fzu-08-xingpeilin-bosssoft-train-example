package com.bosssoft.hr.train.test;

import com.bosssoft.hr.train.bean.User;
import com.bosssoft.hr.train.dao.UserMapper;
import com.bosssoft.hr.train.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--21:53
 */
public class UserMapperTest {
    private static final Logger log = Logger.getLogger(UserMapperTest.class);

    private static final SqlSessionFactory SQL_SESSION_FACTORY = MybatisUtil.getSqlSessionFactory();

    private static final SqlSession SQL_SESSION = SQL_SESSION_FACTORY.openSession();

    /**
     * 测试批量插入
     */

    @Test
    public void testInsertUsers() {
        try {
            UserMapper mapper = SQL_SESSION.getMapper(UserMapper.class);
            List<User> users = new ArrayList<User>();
            User user;
            for (int i = 0; i < 5; i++) {
                user = new User(UUID.randomUUID().toString().substring(0, 5), i % 2);
                users.add(user);
            }
            mapper.insertUsers(users);
            SQL_SESSION.commit();
        } finally {
            SQL_SESSION.close();
        }

    }


    @Test
    public void testSelectUsers() {
        try {
            UserMapper mapper = SQL_SESSION.getMapper(UserMapper.class);
            User user = new User();
            user.setId(1);
            List<User> users = mapper.selectUsersByCondition(user);
            log.info(users);

            user = new User();
            user.setLastName("%a%");
            users = mapper.selectUsersByCondition(user);
            log.info(users);

            user = new User();
            users = mapper.selectUsersByCondition(user);
            log.info(users);

        } finally {
            SQL_SESSION.close();
        }
    }

    @Test
    public void testUpdate() {
        try {
            UserMapper mapper = SQL_SESSION.getMapper(UserMapper.class);
            User user = new User("updateUser", 1);
            user.setId(1);
            mapper.updateUser(user);
            SQL_SESSION.commit();
        } finally {
            SQL_SESSION.close();
        }
    }

}
