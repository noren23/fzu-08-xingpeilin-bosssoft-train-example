package com.bosssoft.hr.train.bean;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--22:10
 */
public class User {
    private Integer id;
    private String lastName;
    private Integer gender;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                '}';
    }

    public User() {
    }

    public User(String lastName, Integer gender) {
        this.lastName = lastName;
        this.gender = gender;
    }
}
