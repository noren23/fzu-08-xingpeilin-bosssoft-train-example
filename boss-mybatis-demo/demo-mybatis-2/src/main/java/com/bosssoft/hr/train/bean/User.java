package com.bosssoft.hr.train.bean;

import java.util.Set;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--20:49
 */
public class User {
    private Integer userId;
    private String userName;


    private Company company;
    private Set<Role> roles;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    public String toStringWithCompany() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", company=" + company +
                '}';
    }


    public String toStringWithRole() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", roles=" + roles +
                '}';
    }


}
