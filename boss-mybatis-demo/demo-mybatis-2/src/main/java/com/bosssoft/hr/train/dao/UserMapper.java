package com.bosssoft.hr.train.dao;

import com.bosssoft.hr.train.bean.User;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--20:47
 */
public interface UserMapper {

    User getUserByIdWithCompany(Integer id);
    User getUserByIdWithRole(Integer id);
}
