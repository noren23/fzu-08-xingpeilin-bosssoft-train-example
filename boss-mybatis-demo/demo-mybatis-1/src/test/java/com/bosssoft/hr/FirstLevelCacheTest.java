package com.bosssoft.hr;

import com.bosssoft.hr.train.bean.User;
import com.bosssoft.hr.train.dao.UserMapper;
import com.bosssoft.hr.train.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.List;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--19:46
 */
public class FirstLevelCacheTest {
    private static final Logger log = Logger.getLogger(FirstLevelCacheTest.class);

    private static final SqlSessionFactory SQL_SESSION_FACTORY = MybatisUtil.getSqlSessionFactory();

    private static final SqlSession SQL_SESSION = SQL_SESSION_FACTORY.openSession();

    @Test
    public void testUserFirstCacheWithOutModify() {
        UserMapper userMapper = SQL_SESSION.getMapper(UserMapper.class);

        List<User> users = userMapper.getUserLikeName("%%");
        log.info(users);
        List<User> users2 = userMapper.getUserLikeName("%%");
        log.info(users2);
    }

    @Test
    public void testUserFirstCacheWithModify() {
        UserMapper userMapper = SQL_SESSION.getMapper(UserMapper.class);
        List<User> users = userMapper.getUserLikeName("%%");
        log.info(users);
        User user = new User(null, "lxp", 22);
        userMapper.insertUser(user);
        List<User> users2 = userMapper.getUserLikeName("%%");
        log.info(users2);
    }


    @Test
    public void testUserFirstCacheWithTwoSqlSession() {
        UserMapper userMapper = SQL_SESSION.getMapper(UserMapper.class);
        List<User> users = userMapper.getUserLikeName("%%");
        log.info(users);
        SqlSession sqlSession2 = SQL_SESSION_FACTORY.openSession();
        UserMapper userMapper2 = sqlSession2.getMapper(UserMapper.class);
        List<User> users2 = userMapper2.getUserLikeName("%%");
        log.info(users2);
    }
}
