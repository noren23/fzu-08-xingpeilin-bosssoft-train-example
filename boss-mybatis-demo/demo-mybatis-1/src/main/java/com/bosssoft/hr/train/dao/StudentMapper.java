package com.bosssoft.hr.train.dao;

import com.bosssoft.hr.train.bean.Student;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--19:36
 */
public interface StudentMapper {
    Student getStudentById(Integer id);

    int insertStudent(Student student);

}
