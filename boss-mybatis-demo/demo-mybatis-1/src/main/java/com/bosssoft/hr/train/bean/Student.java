package com.bosssoft.hr.train.bean;

import java.io.Serializable;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/7--18:34
 */
public class Student implements Serializable {
    private Integer id;
    private Integer score;

    public Student() {
    }

    public Student(Integer id, Integer score) {
        this.id = id;
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", score=" + score +
                '}';
    }
}
