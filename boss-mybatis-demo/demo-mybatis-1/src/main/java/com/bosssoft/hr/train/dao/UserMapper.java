package com.bosssoft.hr.train.dao;

import com.bosssoft.hr.train.bean.User;

import java.util.List;

/**
 * @description:测试缓存要有一个查询和可以更改数据库的语句
 * @author:西西西瓜萌
 * @time:2020/6/7--18:37
 */
public interface UserMapper {
    List<User> getUserLikeName(String name);

    int insertUser(User user);
}
