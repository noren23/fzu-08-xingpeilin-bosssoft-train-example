import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Lists from '../views/Lists.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    component: Login,
  },
    {
      path: '/lists',
      name: 'lists',
      component: Lists,
    },
    { path: '/error', component: () => import('../views/error'), hidden: true }




]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
