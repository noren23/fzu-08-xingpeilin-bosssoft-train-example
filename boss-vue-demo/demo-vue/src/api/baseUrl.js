let baseUrl = "";
switch (process.env.NODE_ENV) {
    case 'dev':
        baseUrl = "http://localhost:8081/"
        break
    case 'serve':
        baseUrl = "http://localhost:8082/"
        break
}

export default baseUrl;