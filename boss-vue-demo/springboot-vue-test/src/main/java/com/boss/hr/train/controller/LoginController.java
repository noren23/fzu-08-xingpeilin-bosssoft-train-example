package com.boss.hr.train.controller;



import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author: Eli Shaw
 * @Date: 2019-11-14 11:33:26
 * @Description：
 */
@RestController
public class LoginController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/login")
    public CommonResult<Object> login(@RequestBody User user) {
        if (user.getUsername().equals("123456")){
            return CommonResult.success("123456");
        }else{
            return CommonResult.validateFailed();
        }

    }

    @PostMapping(value = "/userlist")
    public List<User> userList(){
        return userService.getUserList();
    }


}
