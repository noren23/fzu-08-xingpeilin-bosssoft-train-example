package com.boss.hr.train.service;

import com.boss.hr.train.entity.User;

import java.util.List;

public interface UserService {
    public void save(User user);
    public void remove(int id);
    public void update(User user);
    public List<User> getList(String name);
    public List<User> getUserList();


}
