package com.bosssoft.hr.train.dao;

import com.bosssoft.hr.train.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/13-17:54
 */
@Repository
@Mapper
public interface UserMapper {
    @Select("SELECT * FROM t_user")
    List<User> findAll();

    @Select("SELECT * FROM t_user WHERE id=#{id}")
    User getUserById(Integer id);
}
