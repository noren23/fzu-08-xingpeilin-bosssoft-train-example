package com.bosssoft.hr.train;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author 西西西瓜萌
 */
@SpringBootApplication
@EnableCaching
@MapperScan("com.bosssoft.hr.train.dao")
public class SpringRedisDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringRedisDemo1Application.class, args);
    }

}
