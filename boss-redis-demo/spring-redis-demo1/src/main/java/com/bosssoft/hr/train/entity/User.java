package com.bosssoft.hr.train.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/13-17:52
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 4910459000936618734L;
    private Integer id;
    private String name;
    private Integer age;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
