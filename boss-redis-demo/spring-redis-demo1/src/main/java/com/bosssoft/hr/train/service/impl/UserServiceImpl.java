package com.bosssoft.hr.train.service.impl;

import com.bosssoft.hr.train.dao.UserMapper;
import com.bosssoft.hr.train.entity.User;
import com.bosssoft.hr.train.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/13-17:57
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "user")
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;
    /**
     * 通识符前缀
     */
    private static final String PREFIX = "user_";
    /**
     * 使用redisTemplate手动添加缓存
     *
     * @return
     */
    @Override
    public Object findAll() {
        int cacheTime = 30;
        String cacheKey = "user_list";
        String cacheSign = cacheKey + "_sign";
        String sign = (String) redisTemplate.opsForValue().get(cacheSign);
        Set<Object> keys = redisTemplate.keys(PREFIX + "*");
        List<Object> cacheValue = redisTemplate.opsForValue().multiGet(keys);
        if (sign != null) {
            log.info("redis中存在user，值为：{}",cacheValue);
            return cacheValue;
        } else {
            log.info("redis中不存在user，重新放入");
            redisTemplate.opsForValue().set(cacheSign, "1", cacheTime, TimeUnit.SECONDS);
            List<User> users = userMapper.findAll();
            for (User user : users) {
                int keyCacheTime = cacheTime + new Random().nextInt(cacheTime);
                redisTemplate.opsForValue().set(PREFIX + user.getId(), user,
                        keyCacheTime, TimeUnit.SECONDS);
            }
            return users;
        }
    }

    /**
     * 先去缓存中拿，如果没有去数据库查
     *
     *
     * @param id
     * @return
     */
    @Override
    public User getUserById(Integer id) {
        String cacheKey = PREFIX + id;
        User user = (User) redisTemplate.opsForValue().get(cacheKey);
        if (user != null) {
            log.info("UserServiceImpl.getUserById,从缓存中获取:{}", id);
            return user;
        } else {
            log.info("UserServiceImpl.getUserById，从数据库中查询:{}", id);
            user = userMapper.getUserById(id);
            redisTemplate.opsForValue().set(cacheKey, user != null ? user : new User(), user != null ? 300 : 600, TimeUnit.SECONDS);
            return user;
        }
    }
}
