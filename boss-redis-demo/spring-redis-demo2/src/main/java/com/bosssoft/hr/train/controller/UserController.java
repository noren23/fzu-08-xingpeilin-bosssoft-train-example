package com.bosssoft.hr.train.controller;

import com.bosssoft.hr.train.entity.User;
import com.bosssoft.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @class UserController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/3 15:25
 * @version 1.0.0
 * @see 
 * @since 
 */


@RestController
public class UserController {
    @Autowired
    UserService userService;


    @GetMapping("/get")
    public User getUserById(Integer id) {
        return userService.getUserById(id);
    }
}
