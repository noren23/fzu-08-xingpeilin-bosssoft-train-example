package com.bosssoft.hr.train.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1692718854911078569L;
    private Integer id;
    private String name;
    private Integer age;

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }



}
