package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.entity.User;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/13-21:23
 */
public interface UserService {
    /**
     * @param: id
     * @return: 
     * @see
     * @since
     */
    User getUserById(Integer id);
}
