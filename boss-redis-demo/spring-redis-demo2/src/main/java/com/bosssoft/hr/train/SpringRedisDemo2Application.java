package com.bosssoft.hr.train;
/**
 * @file:  SpringRedisDemo2Application.java    
 * @author: 西西西瓜萌   
 * @date:   2020/8/3 15:25
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved. 
 */ 
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
/**
 * @class SpringRedisDemo2Application
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/3 15:06
 * @version 1.0.0
 * @see 
 * @since 
 */

@MapperScan("com.bosssoft.hr.train.mapper")
@EnableCaching
@SpringBootApplication
public class SpringRedisDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringRedisDemo2Application.class, args);
    }

}
