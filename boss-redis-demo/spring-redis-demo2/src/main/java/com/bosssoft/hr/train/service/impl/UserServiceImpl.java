package com.bosssoft.hr.train.service.impl;

import com.bosssoft.hr.train.entity.User;
import com.bosssoft.hr.train.mapper.UserMapper;
import com.bosssoft.hr.train.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/13-21:23
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    private static final String PREFIX = "lockUser_";
    private static final int CACHE_TIME = 30;
    private static final String LOCK_NAME = "key_mutex";

    /**
     * 方法默认为数据库都有
     *
     * @param id
     * @return
     */


    @Override
    public User getUserById(Integer id) {
        String cacheKey = PREFIX + "id";
        User user = (User) redisTemplate.opsForValue().get(cacheKey);
        if (user == null) {
            Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(LOCK_NAME, id, 180, TimeUnit.SECONDS);
            if (Boolean.TRUE.equals(ifAbsent)) {
                user = userMapper.getUserById(id);
                redisTemplate.opsForValue().set(cacheKey, user, CACHE_TIME, TimeUnit.SECONDS);
                if (redisTemplate.opsForValue().get(LOCK_NAME).equals(id)) {
                    redisTemplate.delete(LOCK_NAME);
                }
            } else {
                tryGetLock(id, 5L);
            }
        }

        return user;
    }

    /**
     * 尝试再次拿到锁,用于第一次没获取到
     *
     * @param id
     * @param seconds
     */
    public void tryGetLock(Integer id, Long seconds) {
        try {
            Thread.sleep(seconds * 60);
            getUserById(id);
        } catch (Exception e) {
            log.warn("线程被迫中断，方法为：{}", "UserServiceImpl.getUserById");
        }
    }


}
