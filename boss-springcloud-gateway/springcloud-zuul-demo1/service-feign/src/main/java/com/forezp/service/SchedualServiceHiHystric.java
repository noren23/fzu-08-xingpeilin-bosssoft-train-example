package com.forezp.service;

import org.springframework.stereotype.Component;

/**
 * @class SchedualServiceHiHystric
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/5 11:12
 * @version 1.0.0
 * @see
 * @since
 */
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
