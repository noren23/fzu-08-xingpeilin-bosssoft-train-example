package com.forezp.web;

import com.forezp.service.SchedualServiceHi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @class HiController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/5 11:12
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
public class HiController {

    @Autowired
    SchedualServiceHi schedualServiceHi;
    @GetMapping(value = "/hi")
    public String sayHi(@RequestParam String name){
        return schedualServiceHi.sayHiFromClientOne(name);
    }
}
