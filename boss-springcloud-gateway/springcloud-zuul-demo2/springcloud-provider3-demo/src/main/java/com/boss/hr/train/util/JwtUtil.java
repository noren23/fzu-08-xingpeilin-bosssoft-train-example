package com.boss.hr.train.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


import java.util.Date;

/**
 * @class JwtUtil
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/5 11:24
 * @version 1.0.0
 * @see
 * @since
 */

@Slf4j
@Component
public class JwtUtil {
    public static final long TOKEN_EXPIRE_TIME = 7200000;
    private static final String ISSUER = "mao";
    private static final Logger log = LoggerFactory.getLogger(JwtUtil.class);


    private JwtUtil(){

    }

      /**
       * 生成Token
       * @param
       * @return
       * @see
       * @since
       */
    public static String genrateToken(String userLable,String secretKey){
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        Date now = new Date();
        Date expireTime = new Date(now.getTime()+TOKEN_EXPIRE_TIME);
        return JWT.create()
                .withIssuer(ISSUER)
                .withIssuedAt(now)
                .withExpiresAt(expireTime)
                .withClaim("userLabel",userLable)
                .sign(algorithm);

    }

}
