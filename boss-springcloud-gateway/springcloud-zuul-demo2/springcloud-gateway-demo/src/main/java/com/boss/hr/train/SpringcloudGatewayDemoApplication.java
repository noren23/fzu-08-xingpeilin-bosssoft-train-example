package com.boss.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
/**
 * @class SpringcloudGatewayDemoApplication
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 11:26
 * @version 1.0.0
 * @see 
 * @since 
 */
@SpringBootApplication
@EnableEurekaClient
public class SpringcloudGatewayDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudGatewayDemoApplication.class, args);
    }

}
