package com.boss.hr.train.myfilter;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
/**
 * @class MyZuulFilter
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 11:38
 * @version 1.0.0
 * @see 
 * @since 
 */
public class MyZuulFilter extends ZuulFilter {
    Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    public String filterType() {
        return null;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s >>> %s", request.getMethod(), request.getRequestURL().toString()));
        Object accessToken = request.getParameter("token");
        if(accessToken == null) {
            log.warn("token is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            try {
                ctx.getResponse().getWriter().write("token is empty");
            }catch (Exception e){
                log.info(e.toString());
            }

            return null;
        }
        log.info("ok");
        return accessToken;

    }
}
