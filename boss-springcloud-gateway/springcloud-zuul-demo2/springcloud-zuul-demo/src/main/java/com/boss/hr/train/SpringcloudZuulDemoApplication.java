package com.boss.hr.train;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 * @class SpringcloudZuulDemoApplication
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 11:38
 * @version 1.0.0
 * @see 
 * @since 
 */
@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
public class SpringcloudZuulDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudZuulDemoApplication.class, args);
    }

}
