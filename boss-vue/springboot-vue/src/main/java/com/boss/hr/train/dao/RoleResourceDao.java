package com.boss.hr.train.dao;

import com.boss.hr.train.entity.CheckedRoleResource;
import com.boss.hr.train.entity.RoleResource;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;


import java.util.List;
/**
 * @class ResourceDao
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/17 21:10
 * @version 1.0.0
 * @see
 * @since
 */
@Mapper
@Component(value = "RoleResourceDao")
public interface RoleResourceDao {
    public void save(RoleResource roleResource);
    public void update(int roleId, int resourceId, int Status);
    public List<Integer> getList(int roleId);

}
