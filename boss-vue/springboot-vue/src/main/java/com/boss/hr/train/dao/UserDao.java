package com.boss.hr.train.dao;


import com.boss.hr.train.entity.RoleResource;
import com.boss.hr.train.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * @class UserDao
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/17 19:17
 * @version 1.0.0
 * @see 
 * @since 
 */
@Mapper
@Component(value = "UserDao")
public interface UserDao {
    public void save(User user);
    public void remove(int id);
    public void update(User user);
    public List<User>getList(String name);
    public List<User>getUserList();

}
