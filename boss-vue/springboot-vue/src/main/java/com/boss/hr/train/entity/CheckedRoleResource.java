package com.boss.hr.train.entity;

import java.util.List;
/**
 * @class CheckedRoleResource
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/18 22:45
 * @version 1.0.0
 * @see
 * @since
 */
public class CheckedRoleResource {
    private int roleId;
    private List<Integer> checkedResourceId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public List<Integer> getCheckedResourceId() {
        return checkedResourceId;
    }

    public void setCheckedResourceId(List<Integer> checkedResourceId) {
        this.checkedResourceId = checkedResourceId;
    }

}
