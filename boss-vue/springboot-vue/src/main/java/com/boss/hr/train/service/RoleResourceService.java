package com.boss.hr.train.service;

import com.boss.hr.train.entity.CheckedRoleResource;
import com.boss.hr.train.entity.RoleResource;

import java.util.List;

/**
 * @class ResourceService
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/17 21:32
 * @version 1.0.0
 * @see
 * @since
 */
public interface RoleResourceService {
    /**
     * 保存
     * @param
     * @return
     * @see
     * @since
     */
    public void save(RoleResource roleResource);
    /**
     * 删除
     * @param
     * @return
     * @see
     * @since
     */
    public void update(int roleId,int resourceId,int status);
    /**
     * 获取列表
     * @param
     * @return
     * @see
     * @since
     */
    public List<Integer> getList(int roleId);

}
