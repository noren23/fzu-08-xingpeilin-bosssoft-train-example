package com.boss.hr.train.controller;

import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.CheckedRoleResource;
import com.boss.hr.train.entity.RoleResource;
import com.boss.hr.train.service.RoleResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @class RoleResourceController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/17 23:45
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
public class RoleResourceController {
    @Autowired
    RoleResourceService roleResourceService;

    @PostMapping(value = "/update1")
    public CommonResult<Object> update(@RequestBody CheckedRoleResource checkedRoleResource){
        for (int i = 1; i <= 20; i++){
            if(checkedRoleResource.getCheckedResourceId().contains(i)){
                roleResourceService.update(checkedRoleResource.getRoleId(), i, 1);
            }
            else{
                roleResourceService.update(checkedRoleResource.getRoleId(), i, 0);
            }
        }
        return CommonResult.success("update");
    }

    @PostMapping(value = "/getRoleResource")
    public List<Integer> getRoleResource(@RequestBody RoleResource roleResource){
        return  roleResourceService.getList(roleResource.getRoleId());
    }



}
