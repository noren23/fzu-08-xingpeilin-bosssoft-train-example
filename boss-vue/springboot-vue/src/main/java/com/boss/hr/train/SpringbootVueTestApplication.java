package com.boss.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/**
 * @class SpringbootVueTestApplication
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/17 19:33
 * @version 1.0.0
 * @see 
 * @since 
 */
@EnableEurekaServer
@SpringBootApplication
public class SpringbootVueTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootVueTestApplication.class, args);
    }

}
