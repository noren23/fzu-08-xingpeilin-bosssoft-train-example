package com.boss.hr.train.api;

/**
 * @class IErrorCode
 * @classdesc 封装API的错误码
 * @author 西西西瓜萌
 * @date 2020/8/17 17:43
 * @version 1.0.0
 * @see
 * @since
 */
public interface IErrorCode {
    long getCode();
    String getMessage();
}
