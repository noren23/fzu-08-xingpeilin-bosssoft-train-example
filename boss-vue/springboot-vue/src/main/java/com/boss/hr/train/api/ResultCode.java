package com.boss.hr.train.api;

/**
 * @class ResultCode
 * @classdesc 枚举了一些常用API操作码
 * @author 西西西瓜萌
 * @date 2020/8/17 17:43
 * @version 1.0.0
 * @see
 * @since
 */
public enum ResultCode implements IErrorCode {
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    FORBIDDEN(403, "没有相关权限");
    private long code;
    private String message;

    private ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
