package com.boss.hr.train.entity;
/**
 * @class RoleResource
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/17 23:45
 * @version 1.0.0
 * @see
 * @since
 */
public class RoleResource {
    private int roleId;
    private int roleResourceId;
    private int resourceId;
    private int Status;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getRoleResourceId() {
        return roleResourceId;
    }

    public void setRoleResourceId(int roleResourceId) {
        this.roleResourceId = roleResourceId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }
}
