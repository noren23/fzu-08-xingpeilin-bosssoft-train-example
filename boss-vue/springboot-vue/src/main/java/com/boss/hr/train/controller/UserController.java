package com.boss.hr.train.controller;

import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.RoleResource;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @class UserController
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/6 11:28
 * @version 1.0.0
 * @see 
 * @since 
 */
@RestController
public class UserController {
    @Autowired
    UserService userService;
    @PostMapping(value = "/save")
    public CommonResult<Object> save(@RequestBody User user){
        try{
            userService.save(user);
        }catch (Exception ex) {
            return CommonResult.failed("500");
        }

        userService.save(user);
        return CommonResult.success("save");
    }

    @PostMapping(value = "/remove")
    public CommonResult<Object> remove(@RequestBody User user){
        userService.remove(user.getId());
        return CommonResult.success("remove");
    }

    @PostMapping(value = "/update")
    public CommonResult<Object> update(@RequestBody User user){
        try{
            userService.update(user);
        } catch (Exception ex) {
            return CommonResult.failed("500");
        }
        return CommonResult.success("update");
    }

    @PostMapping(value = "/query")
    public List<User> getUser(@RequestBody User user){
        return  userService.getList(user.getName());
    }



}
