package com.boss.hr.train.service.impl;

import com.boss.hr.train.dao.RoleResourceDao;
import com.boss.hr.train.entity.CheckedRoleResource;
import com.boss.hr.train.entity.RoleResource;
import com.boss.hr.train.service.RoleResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @class RoleResourceServiceImpl
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/17 23:49
 * @version 1.0.0
 * @see
 * @since
 */
@Service
public class RoleResourceServiceImpl implements RoleResourceService {
    @Autowired
    private RoleResourceDao roleResourceDao;

    @Override
    public void save(RoleResource roleResource) {
        roleResourceDao.save(roleResource);
    }


    @Override
    public void update(int roleId, int resourceId, int Status) {
        roleResourceDao.update(roleId, resourceId, Status);
    }

    @Override
    public List<Integer> getList(int roleId) {
        return roleResourceDao.getList(roleId);
    }



}
