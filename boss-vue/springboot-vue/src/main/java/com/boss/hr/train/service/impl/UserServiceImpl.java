package com.boss.hr.train.service.impl;

import com.boss.hr.train.dao.UserDao;
import com.boss.hr.train.entity.RoleResource;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @class UserServiceImpl
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/17 19:33
 * @version 1.0.0
 * @see 
 * @since 
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public void save(User user) {
            userDao.save(user);
    }


    @Override
    public void remove(int id) {
            userDao.remove(id);
    }

    @Override
    public void update(User user) {
            userDao.update(user);
    }

    @Override
    public List<User> getList(String name) {
        return userDao.getList(name);
    }

    @Override
    public List<User> getUserList() {
        return userDao.getUserList();
    }


}
