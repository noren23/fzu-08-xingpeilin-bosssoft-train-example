package com.boss.hr.train.controller;



import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @class LoginController
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/17 19:11
 * @version 1.0.0
 * @see 
 * @since 
 */
@RestController
public class LoginController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/login")
    public CommonResult<Object> login(@RequestBody User user) {
        if (user.getUsername().equals("admin")){
            return CommonResult.success("123456");
        }else{
            return CommonResult.validateFailed();
        }

    }

    @PostMapping(value = "/userlist")
    public List<User> userList(){
        return userService.getUserList();
    }


}
