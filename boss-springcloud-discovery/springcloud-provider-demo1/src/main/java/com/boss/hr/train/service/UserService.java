package com.boss.hr.train.service;

import com.boss.hr.train.entity.User;

import java.util.List;
/**
 * @class UserService
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/3 21:47
 * @version 1.0.0
 * @see
 * @since
 */
public interface UserService {
    public void save(User user);
    public void remove(int id);
    public void update(User user);
    public List<User> getList(String name);
    public List<User> getUserList();
}
