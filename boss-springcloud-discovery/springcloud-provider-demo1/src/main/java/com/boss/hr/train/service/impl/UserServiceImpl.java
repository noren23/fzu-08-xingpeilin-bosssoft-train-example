package com.boss.hr.train.service.impl;

import com.boss.hr.train.service.UserService;
import com.boss.hr.train.dao.UserDao;
import com.boss.hr.train.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.redis.core.RedisTemplate;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @class UserServiceImpl
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/3 21:49
 * @version 1.0.0
 * @see
 * @since
 */


@Service
public class UserServiceImpl implements UserService {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserDao userDao;
    @Resource
    RedisTemplate<String,Object>redisTemplate;

    /**
    插入
     */
    @Override
    public void save(User user) {
            userDao.save(user);
    }

    /**
    删除
     */

    @Override
    public void remove(int id) {
            userDao.remove(id);
    }

    /**
    修改
     */
    @Override
    public void update(User user) {
            userDao.update(user);
    }

    /**
    查询
     */
    @Override
    public List<User> getList(String name) {
        String key = "user-"+name;
        List<User> list;
        if(Boolean.TRUE.equals(redisTemplate.hasKey(key))){
            log.info("有缓存，从缓存中查询");
            return (List<User>) redisTemplate.opsForValue().get(key);
        }else {
            log.info("无缓存，从数据库查询");
            list = userDao.getList(name);
            if(!list.isEmpty()) {
                redisTemplate.opsForValue().set(key, list, 10, TimeUnit.MINUTES);
            }
        }
        return list;
    }

    @Override
    public List<User> getUserList() {
        String key = "userList";
        List<User> users ;
        if(Boolean.TRUE.equals(redisTemplate.hasKey(key))){
            log.info("从缓存中读取");
            return (List<User>) redisTemplate.opsForValue().get(key);
        } else {
            log.info("从数据库读取");
            users = userDao.getUserList();
            redisTemplate.opsForValue().set(key,users,10, TimeUnit.MINUTES);
        }
        return users;
    }
}
