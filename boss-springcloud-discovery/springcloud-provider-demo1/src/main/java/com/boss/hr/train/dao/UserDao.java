package com.boss.hr.train.dao;


import com.boss.hr.train.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper
@Component(value = "UserDao")
public interface UserDao {
    /**
     * 保存
     * @param: user
     * @return:
     * @see
     * @since
     */
    public void save(User user);
    /**
     * 删除
     * @param: id
     * @return:
     * @see
     * @since
     */
    public void remove(int id);
    /**
     * 更新
     * @param: user
     * @return:
     * @see
     * @since
     */
    public void update(User user);
    /**
     * 通过name获取
     * @param: name
     * @return:
     * @see
     * @since
     */
    public List<User>getList(String name);
    /**
     * 获取列表
     * @param:
     * @return:
     * @see
     * @since
     */
    public List<User>getUserList();
}
