package com.boss.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringcloudDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemo1Application.class, args);
    }

}
