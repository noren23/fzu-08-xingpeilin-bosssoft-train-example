package com.boss.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/**
 * @class SpringcloudDemo1Application
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:45
 * @version 1.0.0
 * @see
 * @since
 */
@SpringBootApplication
@EnableEurekaServer
public class SpringcloudDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudDemo1Application.class, args);
    }

}
