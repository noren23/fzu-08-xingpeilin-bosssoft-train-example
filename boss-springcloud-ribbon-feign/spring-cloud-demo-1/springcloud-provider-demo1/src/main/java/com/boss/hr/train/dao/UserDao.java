package com.boss.hr.train.dao;


import com.boss.hr.train.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * @class UserDao
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:52
 * @version 1.0.0
 * @see
 * @since
 */
@Mapper
@Component(value = "UserDao")
public interface UserDao {
    /**
     * 插入用户
     * @param user
     * @return
     * @see
     * @since
     */
    public void save(User user);
    /**
     * 删除用户
     * @param id
     * @return
     * @see
     * @since
     */
    public void remove(int id);
    /**
     * 修改用户
     * @param user
     * @return
     * @see
     * @since
     */
    public void update(User user);
    /**
     * 查询用户
     * @param name
     * @return
     * @see
     * @since
     */
    public List<User>getList(String name);
    /**
     * 加载页面查询所有用户
     * @param
     * @return
     * @see
     * @since
     */
    public List<User>getUserList();
}
