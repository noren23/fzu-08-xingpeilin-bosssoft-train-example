package com.boss.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
/**
 * @class SpringcloudProvider2Demo1Application
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 21:00
 * @version 1.0.0
 * @see
 * @since
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCaching
public class SpringcloudProvider2Demo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProvider2Demo1Application.class, args);
    }

}
