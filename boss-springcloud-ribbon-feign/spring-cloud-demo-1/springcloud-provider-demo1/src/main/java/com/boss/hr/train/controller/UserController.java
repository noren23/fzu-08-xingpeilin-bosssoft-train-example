package com.boss.hr.train.controller;

import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @class UserController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:52
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
public class UserController {
    @Autowired
    UserService userService;
    /**
     * 插入
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/providerSave")
    public CommonResult<Object> save(@RequestBody User user){
        userService.save(user);
        return CommonResult.success("save");
    }

    /**
     * 删除
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/providerRemove")
    public CommonResult<Object> remove( int id){
        userService.remove(id);
        return CommonResult.success("remove");
    }

    /**
     * 修改
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/providerUpdate")
    public CommonResult<Object> update(@RequestBody User user){
        userService.update(user);
        return CommonResult.success("update");
    }

    /**
     * 查询
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/providerQuery")
    public List<User> getUser(String name){
        System.err.println(name);
        return  userService.getList(name);
    }



}
