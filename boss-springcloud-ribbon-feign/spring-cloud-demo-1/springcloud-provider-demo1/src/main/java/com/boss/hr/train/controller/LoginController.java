package com.boss.hr.train.controller;



import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @class LoginController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 21:01
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
public class LoginController {

    @Autowired
    UserService userService;

    /**
     * 登录
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/login")
    public CommonResult<Object> login(@RequestBody User user) {
        if (user.getUsername().equals("123456")){
            return CommonResult.success("123456");
        }else{
            return CommonResult.validateFailed();
        }

    }

    /**
     * 获取列表
     * @param
     * @return
     * @see
     * @since
     */
    @GetMapping(value = "/providerUserlist")
    public List<User> userList(){
        return userService.getUserList();
    }


}
