package com.boss.hr.train;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @class SpringcloudProvider2Demo1ApplicationTests
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 21:01
 * @version 1.0.0
 * @see
 * @since
 */
@SpringBootTest
class SpringcloudProvider2Demo1ApplicationTests {

    @Test
    void contextLoads() {
    }

}
