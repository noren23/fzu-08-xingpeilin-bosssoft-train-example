package com.boss.hr.train.controller;

import com.boss.hr.train.api.CommonResult;
import com.boss.hr.train.entity.User;
import com.boss.hr.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * @class UserController
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:41
 * @version 1.0.0
 * @see
 * @since
 */
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    UserService userService;
    /**
     * 插入
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/save")
    public CommonResult<Object> save(@RequestBody User user){
        userService.save(user);
        return CommonResult.success("save");
    }

    /**
     * 删除
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/remove")
    public CommonResult<Object> remove(@RequestBody User user){
        userService.remove(user.getId());
        return CommonResult.success("remove");
    }

   /**
    * 修改
    * @param
    * @return
    * @see
    * @since
    */
    @PostMapping(value = "/update")
    public CommonResult<Object> update(@RequestBody User user){
        userService.update(user);
        return CommonResult.success("update");
    }

    /**
     * 查询
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/query")
    public List<User> getUser(@RequestBody User user){
        return  userService.getList(user.getName());
    }


    /**
     * 登录
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/login")
    public CommonResult<Object> login(@RequestBody User user) {
        if (user.getUsername().equals("123456")){
            return CommonResult.success("123456");
        }else{
            return CommonResult.validateFailed();
        }

    }

    /**
     * 获取列表
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping(value = "/userlist")
    public List<User> userList(){
        return userService.getUserList();
    }
}
