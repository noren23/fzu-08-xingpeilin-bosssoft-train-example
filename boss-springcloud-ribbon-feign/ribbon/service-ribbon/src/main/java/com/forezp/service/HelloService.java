package com.forezp.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @class HelloService
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:43
 * @version 1.0.0
 * @see
 * @since
 */
@Service
public class HelloService {

    @Autowired
    RestTemplate restTemplate;

    public String hiService(String name) {
        return restTemplate.getForObject("http://SERVICE-HI/hi?name="+name,String.class);
    }

}
