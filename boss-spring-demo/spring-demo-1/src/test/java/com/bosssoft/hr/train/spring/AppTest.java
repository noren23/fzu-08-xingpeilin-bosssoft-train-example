package com.bosssoft.hr.train.spring;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AppTest {
    App app;

    @Before
    public void setUp() {
        app = new App();
    }

    @After
    public void tearDown() {
        app = null;
    }

    @Test
    public void testByBeanFactory() {
        app.testByBeanFactory();
    }

    @Test
    public void testByClassPathXmlApplicationContext() {
        app.testByClassPathXmlApplicationContext();
    }
}