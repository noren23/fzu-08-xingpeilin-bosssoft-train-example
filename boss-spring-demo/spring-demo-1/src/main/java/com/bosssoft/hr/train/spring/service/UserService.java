package com.bosssoft.hr.train.spring.service;

/**
 * @author 西西西瓜萌
 */
public interface UserService {
    /**
     * 初始化方法
     */
    void init();

    /**
     * 销毁方法
     */
    void destroy();
}
