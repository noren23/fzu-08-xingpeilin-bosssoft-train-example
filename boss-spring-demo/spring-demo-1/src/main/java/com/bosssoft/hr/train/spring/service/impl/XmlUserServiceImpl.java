package com.bosssoft.hr.train.spring.service.impl;

import com.bosssoft.hr.train.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;

/**
 * @description: 通过xml
 * @author 西西西瓜萌
 * @time:2020/6/4--15:24
 */
@Slf4j
public class XmlUserServiceImpl implements UserService {
    @Override
    public void init() {
        log.info("XmlUserServiceImpl通过xml的方式执行了init");
    }

    @Override
    public void destroy() {
        log.info("XmlUserServiceImpl通过xml的方式执行了destroy");
    }

    public XmlUserServiceImpl() {
        log.info("XmlUserServiceImpl创建了");
    }
}
