package com.bosssoft.hr.train.spring;

import com.bosssoft.hr.train.spring.service.impl.XmlUserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * @description:
 * @author 西西西瓜萌
 * @time:2020/6/4--15:13
 */
@Slf4j
public class App {
    private static final String PATH = "applicationContext.xml";

    /**
     * 测试BeanFactory
     */
    public void testByBeanFactory() {
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource(PATH));
        log.info("beanFactory创建");
        log.info("准备执行userService");
        XmlUserServiceImpl userService = beanFactory.getBean(XmlUserServiceImpl.class);
        log.info("userService:{}", userService);

    }

    /**
     * 测试ClassPathXmlApplicationContext
     */
    public void testByClassPathXmlApplicationContext() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(PATH);
        log.info("applicationContext创建");


        applicationContext.close();
        log.info("applicationContext关闭");
    }

}
