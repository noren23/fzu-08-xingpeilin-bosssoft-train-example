package com.bosssoft.hr.train.spring.service.impl;

import com.bosssoft.hr.train.spring.bean.Student;
import com.bosssoft.hr.train.spring.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description test
 * @Date 2020/6/6 23:22
 * @Author 西西西瓜萌
 */
@Slf4j
public class StudentServiceImplTest {

    private ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    StudentService service = context.getBean(StudentService.class);

    @Autowired
    public void setService(StudentService service) {
        this.service = service;
    }


    @Test
    public void testCache() {
        Student student = new Student("1", "lxp", 22);
        log.info("添加:" + service.saveStudent(student));
        log.info(service.getStudentById("1") + "");
        log.info(service.getStudentById("1") + "");
        student.setName("zyx");
        log.info("修改"+service.updateStudent(student));
        log.info("修改"+service.updateStudent(student));
        log.info("删除:" + service.removeStudent(new Student("1")));
        log.info(service.getStudentById("1") + "");
    }

}