package com.bosssoft.hr.train.spring.service;

import com.bosssoft.hr.train.spring.bean.Student;



/**
 * @Description student业务接口
 * @Date 2020/6/6 15:45
 * @Author 西西西瓜萌
 */
public interface StudentService {

    Student getStudentById(String id);



    Student saveStudent(Student student);

    Student removeStudent(Student student);

    Student updateStudent(Student student);

    boolean[] removeStudent(Student... students);
}
