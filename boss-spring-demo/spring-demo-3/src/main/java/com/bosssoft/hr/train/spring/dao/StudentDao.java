package com.bosssoft.hr.train.spring.dao;

import com.bosssoft.hr.train.spring.bean.Student;


/**
 * @Description DAO接口
 * @Date 2020/6/6 15:41
 * @Author 西西西瓜萌
 */
public interface StudentDao {
    Student queryById(String id);

    int insert(Student student);

    int update(Student student);

    int deleteById(String id);

    int[] deleteByIds(Student... students);

}
