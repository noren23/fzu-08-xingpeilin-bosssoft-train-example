package com.bosssoft.hr.train.spring.exception;

/**
 * @Description 学生异常类 回滚异常
 * @Date 2020/6/6 21:31
 * @Author 西西西瓜萌
 */
public class StudentException extends Exception {
    public StudentException() {
        super();
    }

    public StudentException(String message) {
        super(message);
    }
}
