package com.bosssoft.hr.train.springmvc.dao.impl;

import com.bosssoft.hr.train.springmvc.bean.User;
import com.bosssoft.hr.train.springmvc.dao.UserDao;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/5--20:07
 */
@Repository
public class UserDaoImpl implements UserDao {
    @Override
    public User getUser() {
        User user = new User();
        user.setId(1117);
        user.setName("lxp");
        return user;
    }
}
