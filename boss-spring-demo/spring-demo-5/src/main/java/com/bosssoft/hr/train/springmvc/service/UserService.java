package com.bosssoft.hr.train.springmvc.service;

import com.bosssoft.hr.train.springmvc.bean.User;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/5--20:04
 */
public interface UserService {
    /**
     *
     * @return
     */
    User getUser();
}
