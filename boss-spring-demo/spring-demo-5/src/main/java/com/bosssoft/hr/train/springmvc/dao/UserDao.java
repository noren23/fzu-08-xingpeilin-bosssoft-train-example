package com.bosssoft.hr.train.springmvc.dao;

import com.bosssoft.hr.train.springmvc.bean.User;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/5--20:07
 */
public interface UserDao {
    /**
     *
     * @return
     */
    User getUser();
}
