package com.bosssoft.hr.train.spring;

import com.bosssoft.hr.train.spring.service.impl.UserServicePrototypeImpl;
import com.bosssoft.hr.train.spring.service.impl.UserServiceSingletonImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/4--16:38
 */
@Slf4j
public class App {
    private AnnotationConfigApplicationContext annotationConfigApplicationContext;

    public App() {
        annotationConfigApplicationContext = new AnnotationConfigApplicationContext("com.bosssoft.hr.train.spring");
    }


    public void testSingleton() {
        for (int i = 1; i < 4; i++) {
            log.info("第{}次获取SingletonBean:{}", i, annotationConfigApplicationContext.getBean(UserServiceSingletonImpl.class));
        }
    }


    public void testPrototype() {
        for (int i = 1; i < 4; i++) {
            log.info("第{}次获取PrototypeBean:{}", i, annotationConfigApplicationContext.getBean(UserServicePrototypeImpl.class));
        }
    }
}
