package com.bosssoft.hr.train.spring;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {
    App app;

    @Before
    public void setUp() {
        app = new App();
    }

    @After
    public void tearDown() {
        app = null;
    }

    @Test
    public void testSingleton() {
        app.testSingleton();
    }

    @Test
    public void testPrototype() {
        app.testPrototype();
    }
}