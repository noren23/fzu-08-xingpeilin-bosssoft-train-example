package com.bosssoft.hr.train.spring.service.impl;

import com.bosssoft.hr.train.spring.bean.User;
import com.bosssoft.hr.train.spring.dao.UserDao;
import com.bosssoft.hr.train.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description:UserService的实现
 * @author:西西西瓜萌
 * @time:2020/6/4--21:10
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    @Override
    public int insert(User user) {
        return userDao.insert(user);
    }

    @Override
    public int update(User user) {
        return userDao.update(user);
    }

    @Override
    public int deleteById(Integer id) {
        return userDao.deleteById(id);
    }

    @Override
    public List<User> queryUsers(String name) {
        return userDao.queryUsers(name);
    }

    /**
     * 批量删除开启事务管理，如果失败回滚
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor=Exception.class)
    public int deleteByArray(int... ids) {
        return userDao.deleteByArray(ids);
    }

}
