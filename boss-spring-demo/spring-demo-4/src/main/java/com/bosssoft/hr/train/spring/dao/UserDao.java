package com.bosssoft.hr.train.spring.dao;

import com.bosssoft.hr.train.spring.bean.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * @description:
 * @author:西西西瓜萌
 * @time:2020/6/4--20:22
 */
@Repository
public interface UserDao {
    /**
     * 插入
     * @param user
     * @return
     */
    Integer insert(User user);

    /**
     * 更新
     * @param user
     * @return
     */

    Integer update(User user);

    /**
     * 通过id删除
     * @param id
     * @return
     */
    Integer deleteById(Integer id);

    /**
     * 通过名字模糊搜索
     *
     * @param name
     * @return
     */
    List<User> queryUsers(String name);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    Integer deleteByArray(@Param("ids") int[] ids);
}
