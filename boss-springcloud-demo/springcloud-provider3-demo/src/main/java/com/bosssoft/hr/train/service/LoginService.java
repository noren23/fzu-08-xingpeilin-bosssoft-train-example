package com.bosssoft.hr.train.service;

import org.springframework.stereotype.Service;
/**
 * @class LoginService
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 22:42
 * @version 1.0.0
 * @see 
 * @since 
 */
@Service
public class LoginService {

    private static final String CONSTANT_NUM = "123456";
    public boolean login(String name,String pwd){
        return name.equals(CONSTANT_NUM)&&pwd.equals(CONSTANT_NUM);
    }

    public boolean logout(String name){
        return name.equals(CONSTANT_NUM);
    }
}
