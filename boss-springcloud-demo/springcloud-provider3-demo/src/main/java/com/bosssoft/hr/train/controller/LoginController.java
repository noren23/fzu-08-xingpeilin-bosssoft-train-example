package com.bosssoft.hr.train.controller;


import com.bosssoft.hr.train.service.LoginService;
import com.bosssoft.hr.train.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Value("${secretKey:123456}")
    private String secretKey;

    @Autowired
    private StringRedisTemplate template;

    Logger log = LoggerFactory.getLogger(LoginController.class);
    private static final String TOKEN = "token";

    @GetMapping("/login")
    public String login(@RequestParam("name") String name,@RequestParam("pwd") String pwd){
        boolean res = loginService.login(name,pwd);
        if(res){
            if(template.opsForHash().get(name,TOKEN)!=null){
                return "已登，无需再次登录";
            }else {
                String token = JwtUtil.genrateToken(name,secretKey);
                String fresh = UUID.randomUUID().toString().replace("-","");
                template.opsForHash().put(name,TOKEN,token);
                template.opsForHash().put(name,"refreshToken",fresh);
                template.expire(name,JwtUtil.TOKEN_EXPIRE_TIME, TimeUnit.MILLISECONDS);
                return "登陆成功，token为："+token;
            }

        }
        return "登陆失败";
    }

    @GetMapping("/logout")
    public String logout(@RequestParam("name")String name){
        boolean res = loginService.logout(name);
        if(res){
            if(template.opsForHash().get(name,TOKEN)==null){
                return "未登录，不需要退出";
            }
            else {
                log.info(template.opsForHash().get(name,TOKEN).toString());
                template.opsForHash().delete(name,TOKEN);
                return "成功退出登录";
            }

        }
        return  "用户名错误，请重新输入用户名";
    }

}
