package com.bosssoft.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
/**
 * @class SpringcloudProvider3DemoApplication
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 21:48
 * @version 1.0.0
 * @see 
 * @since 
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCaching
public class SpringcloudProvider3DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProvider3DemoApplication.class, args);
    }

}
