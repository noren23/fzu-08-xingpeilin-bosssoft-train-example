package com.bosssoft.hr.train.service.impl;

import com.bosssoft.hr.train.dao.UserDao;
import com.bosssoft.hr.train.entity.User;
import com.bosssoft.hr.train.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.redis.core.RedisTemplate;
import javax.annotation.Resource;
import java.util.List;

/**
 * @class UserServiceImpl
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:54
 * @version 1.0.0
 * @see
 * @since
 */


@Service
public class UserServiceImpl implements UserService {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserDao userDao;
    @Resource
    RedisTemplate<String,Object>redisTemplate;

    /**
     * 插入
     * @param user
     * @return
     * @see
     * @since
     */
    @Override
    public void save(User user) {
        userDao.save(user);
    }

    /**
     * 删除
     * @param id
     * @return
     * @see
     * @since
     */

    @Override
    public void remove(int id) {
        userDao.remove(id);
    }

    /**
     * 修改
     * @param user
     * @return
     * @see
     * @since
     */
    @Override
    public void update(User user) {
        userDao.update(user);
    }

    /**
     * 查询
     * @param name
     * @return
     * @see
     * @since
     */
    @Override
    public List<User> getList(String name) {
        return userDao.getList(name);
    }

    @Override
    public List<User> getUserList() {
        return userDao.getUserList();



    }


}
