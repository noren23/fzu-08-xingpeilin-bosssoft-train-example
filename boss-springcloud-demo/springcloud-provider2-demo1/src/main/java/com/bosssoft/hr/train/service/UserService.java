package com.bosssoft.hr.train.service;

import com.bosssoft.hr.train.entity.User;

import java.util.List;
/**
 * @class UserService
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/4 20:55
 * @version 1.0.0
 * @see
 * @since
 */
public interface UserService {

    /**
     * 插入用户
     * @param user
     * @return
     * @see
     * @since
     */
    public void save(User user);
    /**
     * 删除用户
     * @param id
     * @return
     * @see
     * @since
     */
    public void remove(int id);
    /**
     * 修改用户
     * @param user
     * @return
     * @see
     * @since
     */
    public void update(User user);
    /**
     * 查询用户
     * @param name
     * @return
     * @see
     * @since
     */
    public List<User> getList(String name);
    /**
     * 获取所有信息
     * @param
     * @return
     * @see
     * @since
     */
    public List<User> getUserList();
}

