package com.bosssoft.hr.train;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @class SpringcloudConfigDemoApplicationTests
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 21:39
 * @version 1.0.0
 * @see 
 * @since 
 */
@SpringBootTest
class SpringcloudConfigDemoApplicationTests {

    @Test
    void contextLoads() {
    }

}
