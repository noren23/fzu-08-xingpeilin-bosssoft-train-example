package com.bossoft.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
/**
 * @class SpringcloudConfigDemoApplication
 * @classdesc
 * @author 西西西瓜萌
 * @date 2020/8/5 21:25
 * @version 1.0.0
 * @see
 * @since
 */
@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
public class SpringcloudConfigDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudConfigDemoApplication.class, args);
    }

}
