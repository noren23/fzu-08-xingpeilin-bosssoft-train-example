package com.bosssoft.hr.train.api;

/**
 * @class IErrorCode
 * @classdesc 封装API的错误码
 * @author 西西西瓜萌
 * @date 2020/8/5 21:24
 * @version 1.0.0
 * @see
 * @since
 */
public interface IErrorCode {
    /**
     * 错误码
     * @param
     * @return
     * @see
     * @since
     */
    long getCode();
    /**
     * 错误信息
     * @param
     * @return
     * @see
     * @since
     */
    String getMessage();
}
