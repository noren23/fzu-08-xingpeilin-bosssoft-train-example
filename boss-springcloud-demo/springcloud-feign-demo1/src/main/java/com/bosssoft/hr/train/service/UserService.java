package com.bosssoft.hr.train.service;
import com.bosssoft.hr.train.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;


@Service
@FeignClient("provider")
public interface UserService {

    /**
     * 插入用户
     * @param user
     * @return
     * @see
     * @since
     */
    @PostMapping("/providerSave")
    void save(@RequestBody User user);
    /**
     * 删除用户
     * @param id
     * @return
     * @see
     * @since
     */
    @PostMapping( "/providerRemove")
    void remove(@RequestParam("id")int id);
    /**
     * 修改用户
     * @param user
     * @return
     * @see
     * @since
     */
    @PostMapping("/providerUpdate")
    void update(@RequestBody  User user);
    /**
     * 查询用户
     * @param name
     * @return
     * @see
     * @since
     */
    @PostMapping("/providerQuery")
    List<User> getList(@RequestParam("name") String name);
    /**
     * 获取列表
     * @param
     * @return
     * @see
     * @since
     */
    @PostMapping("/providerUserlist")
    List<User> getUserList();

}
