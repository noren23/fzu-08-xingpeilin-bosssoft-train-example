package com.bosssoft.hr.train;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
/**
 * @class SpringcloudFeignDemo1Application
 * @classdesc 
 * @author 西西西瓜萌
 * @date 2020/8/5 21:42
 * @version 1.0.0
 * @see 
 * @since 
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SpringcloudFeignDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudFeignDemo1Application.class, args);
    }

}
